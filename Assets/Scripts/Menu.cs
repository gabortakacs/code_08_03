﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
	[SerializeField] private GameObject _menuParent;
	[SerializeField] private Button _startButton;
	[SerializeField] private int _startLevelIndex = 1;

	private void Awake()
	{
		_startButton.onClick.AddListener(OnStartPressed);
	}

	private void OnStartPressed()
	{
		_menuParent.SetActive(false);
		GameMaster.Instance.ChangeLevel(_startLevelIndex);
	}
}
