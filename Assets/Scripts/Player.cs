﻿using System;
using UnityEngine;

public class Player : Singleton<Player>
{
	[SerializeField] private Rigidbody _rigidBody;
	[SerializeField] private float _speed = 100;

	private float _verticalMovementInput;
	private float _horizontalMovementInput;

	private void Update()
	{
		_verticalMovementInput = Input.GetAxis("Vertical");	
		_horizontalMovementInput = Input.GetAxis("Horizontal");	
	}

	void FixedUpdate()
    {
		MovePlayer();
    }

	private void MovePlayer()
	{
		var horizontal = Vector3.right * _horizontalMovementInput * Time.fixedDeltaTime * _speed;
		var vertical = Vector3.up * _verticalMovementInput * Time.fixedDeltaTime * _speed;
		_rigidBody.AddForce(horizontal + vertical, ForceMode.VelocityChange);
	}
}
