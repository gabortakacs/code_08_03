﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
	[SerializeField] private Slider _slider;

	private float _progress;

	public float Progress
	{
		//get => _progress;
		get
		{
			return _progress;
		}
		set
		{
			_progress = value;
			_slider.value = value;
		}
	}

	private void OnEnable()
	{
		Progress = 0.0f;
	}

}
