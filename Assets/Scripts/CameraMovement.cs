﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
	[SerializeField] private float _speed;
	[SerializeField] private Transform _endPosition;
    void FixedUpdate()
    {
		transform.position = Vector3.MoveTowards(transform.position, _endPosition.position, _speed * Time.fixedDeltaTime);
    }
}
