﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : Singleton<GameMaster>
{
	[SerializeField] private LoadingScreen _loadingScreen;

	public void ChangeLevel(int buildIndex)
	{
		StartCoroutine(LoadSceneRoutin(buildIndex));
	}

	private IEnumerator LoadSceneRoutin(int buildIndex)
	{
		_loadingScreen.gameObject.SetActive(true);
		var operation = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
		while(!operation.isDone)
		{
			UpdateLoadingScreen(operation);
			yield return null;
		}
		_loadingScreen.gameObject.SetActive(false);
	}

	private void UpdateLoadingScreen(AsyncOperation operation)
	{
		_loadingScreen.Progress = operation.progress / 0.9f;
	}
}
